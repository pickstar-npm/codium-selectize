import Selectize from 'selectize'

Selectize.define('infinite_scroll', function(options) {
	var self = this

  self.infinitescroll = {
    onScroll: function() {
      var scrollBottom = self.$dropdown_content[0].scrollHeight - (self.$dropdown_content.scrollTop() + self.$dropdown_content.height())
      if(scrollBottom < 300){
        self.$dropdown_content.off('scroll')
        self.onLoadMore(self.lastValue)
      }
    }
  };

  self.onLoadMore = function(value) {
    var fn = self.settings.loadMore;
    if (!fn) return;
    self.load(function(callback) {
      fn.apply(self, [value, callback]);
    });
  }

  self.on('initialize', function() {
    self.$dropdown_content.on('scroll', self.infinitescroll.onScroll);
  })

  self.on('load',function(){
    self.$dropdown_content.on('scroll', self.infinitescroll.onScroll);
  });

});