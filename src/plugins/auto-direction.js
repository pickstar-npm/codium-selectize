import Selectize from 'selectize'
import $ from 'jquery'

Selectize.define('auto_direction', function (options) {
  let self = this

  /**
   * Calculates and applies the appropriate position of the dropdown.
   *
   * Supports dropdownDirection up, down and auto. In case menu can't be fitted it's
   * height is limited to don't fall out of display.
   */
  this.positionDropdown = (function () {
    return function () {
      let $control = this.$control
      let $dropdown = this.$dropdown
      let p = getPositions()

      let direction = getDropdownDirection(p)

      if (direction === 'up') {
        $dropdown.addClass('direction-up').removeClass('direction-down')
      } else {
        $dropdown.addClass('direction-down').removeClass('direction-up')
      }

      $control.attr('data-dropdown-direction', direction)

      let isParentBody = this.settings.dropdownParent === 'body'
      let offset = isParentBody ? $control.offset() : $control.position()
      let fittedHeight

      switch (direction) {
        case 'up':
          offset.top -= p.dropdown.height

          if (p.dropdown.height > p.control.above) {
            fittedHeight = p.control.above - 15
          }
          break

        case 'down':
          offset.top += p.control.height

          if (p.dropdown.height > p.control.below) {
            fittedHeight = p.control.below - 15
          }
          break
      }

      if (fittedHeight) {
        this.$dropdown_content.css({ 'max-height': fittedHeight })
      }

      this.$dropdown.css({
        width: $control.outerWidth(),
        top: offset.top,
        left: offset.left
      })
    }
  })()

  /**
   * Force a re-position when dropdown list is updated
   */
  this.refreshOptions = (function () {
    let original = self.refreshOptions

    return function () {
      original.apply(this, arguments)

      self.positionDropdown()
    }
  })()

  /**
   * Gets direction to display dropdown in. Either up or down.
   */
  function getDropdownDirection(positions) {
    let direction = self.settings.dropdownDirection

    if (direction === 'auto') {
      // down if dropdown fits
      if (positions.control.below > positions.dropdown.height) {
        direction = 'down'

        // otherwise direction with most space
      } else {
        direction = (positions.control.above > positions.control.below) ? 'up' : 'down'
      }
    }

    return direction
  }

  /**
   * Get position information for the control and dropdown element.
   */
  function getPositions() {
    let $control = self.$control
    let $window = $(window)

    let controlHeight = $control.outerHeight(false)
    let controlAbove = $control.offset().top - $window.scrollTop()
    let controlBelow = $window.height() - controlAbove - controlHeight

    let dropdownHeight = self.$dropdown.outerHeight(false)

    return {
      control: {
        height: controlHeight,
        above: controlAbove,
        below: controlBelow
      },
      dropdown: {
        height: dropdownHeight
      }
    }
  }
})
