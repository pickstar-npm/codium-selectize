import Selectize from 'selectize'

Selectize.define('disable_delete', function (options) {
  let self = this

  this.deleteSelection = (function () {
    let original = self.deleteSelection

    return function (event) {
      let returnValue

      if (self.settings.disableDelete && event && event.keyCode === 8) {
        returnValue = null
      } else {
        returnValue = original.apply(this, arguments)
      }

      this.onSearchChange('')

      return returnValue
    }
  })()
})
