## Selectize

A Vue wrapper component for [Selectize](http://selectize.github.io/selectize.js/).

### Installation

Add package via Yarn:

```
$ yarn add git+ssh://git@code.codium.com.au:vue-components/selectize.git
```

Import default styles to your global `scss` file for the project:

```
@import "codium-selectize/dist/codium-selectize.css";
```

Overwrite the styles as necessary.

### Usage

Import the component where you need it:

```
import Selectize from 'codium-selectize'
```

Define it in your components list:

```
components: {
    Selectize
}
```

Use it in your templates:

```
<selectize name="users" :options="users" />
```

#### Options

##### `placeholder`

Type: `string`
Default: `' '`

The placeholder to show when no option is selected.

##### `allowNewOptions`

Type: `boolean`
Default: `false`

Determines if new options can be added by typing into the select box.

##### `onNewOption`

Type: `Function`
Default: `null`

Callback for when a new option is added. Receives `input` as only argument. Callback is expected to return a `Promise`.

##### `searchable`

Type: `boolean`
Default: `true`

Determines if the options can be searched by typing into the select box.

##### `disabled`

Type: `boolean`
Default: `false`

Determines if the select box is disabled.

##### `renderOption`

Type: `Function`
Default: `null`

Callback function that renders an available option in the select box. Receives `item` as first argument and `escape` function as second argument.
Function is expected to return a string or DOM element.

##### `renderItem`

Type: `Function`
Default: `null`

Callback function that renders the selected item in the select box. Receives `item` as first argument and `escape` function as second argument.
Function is expected to return a string or DOM element.

##### `fetchResults`

Type: `Function`
Default: `null`

Callback function that is called when new options need to be loaded from the server. Receives `query` as first argument and `callback` function as second argument.
The `callback` function must be called and given the results of the server response.

Note: Avoid passing in Vuex state directly to Selectize component. Initial results should be local state and always pass result of server to the `callback` instead of
updating local state or Vuex state.

##### `searchField`

Type: `string`
Default: `text`

The name of the field to search over.

##### `disableDelete`

Determines if the delete key should be disabled to prevent unselecting an option.

##### `preLoad`

Type: `boolean`
Default: `false`

Determines if `fetchResults` should be called initially.

#### Install Component Globally

You can install the component globally so it does not need to be done for every component you use it. In a main file:

```
import Vue from 'vue'
import Selectize from 'codium-selectize'

Vue.component('selectize', Selectize)
```
