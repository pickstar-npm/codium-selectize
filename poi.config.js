module.exports = {
  entry: './src/CodiumSelectize.vue',
  filename: {
    js: 'codium-selectize.js',
    css: 'codium-selectize.css'
  },
  sourceMap: false,
  html: false,
  format: 'cjs'
}
